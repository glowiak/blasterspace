package server;

import java.io.InputStream;
import java.io.OutputStream;
import java.io.PrintWriter;
import java.io.OutputStreamWriter;
import java.io.IOException;
import java.net.ServerSocket;
import java.net.Socket;
import java.util.Scanner;
import java.util.ArrayList;

public class ServerThread implements Runnable
{
    private Socket socket;
    private Main superior;
    
    public ServerThread(Main superior, Socket socket)
    {
        this.socket = socket;
        this.superior = superior;
        
        this.superior.PlayerCount++;
    }
    
    @Override
    public void run()
    {
        try {
            InputStream inputToServer = this.socket.getInputStream();
            OutputStream outputFromServer = this.socket.getOutputStream();

            Scanner scanner = new Scanner(inputToServer, "UTF-8");
            PrintWriter serverPrintOut = new PrintWriter(new OutputStreamWriter(outputFromServer, "UTF-8"), true);

            serverPrintOut.println("Hello World");

            boolean done = false;

            while(!done && scanner.hasNextLine())
            {
                String line = scanner.nextLine();
                serverPrintOut.println("> " + line);

                if(line.toLowerCase().trim().equals("exit")) {
                    done = true;
                }
            }
        } catch (IOException ioe) { ioe.printStackTrace(); }
    }
}
