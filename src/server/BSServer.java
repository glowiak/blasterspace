package server;

import java.net.Socket;
import java.net.ServerSocket;
import java.net.InetSocketAddress;
import java.nio.ByteBuffer;
import java.nio.channels.Selector;
import java.nio.channels.SelectionKey;
import java.nio.channels.SocketChannel;
import java.nio.channels.ServerSocketChannel;

import java.util.Set;
import java.util.Iterator;

import java.io.IOException;

public class BSServer implements Runnable
{
    private int port;
    private boolean running = false;
    private Selector selector;
    private ServerSocket ssocket;
    private ByteBuffer buffer;
    
    public static void main(String[] args)
    {
        new BSServer(9991, 4096);
    }
    
    public BSServer(int port, int bufferSize)
    {
        this.port = port;
        this.buffer = ByteBuffer.allocate(bufferSize);
        
        new Thread(this).start();
    }
    
    @Override
    public void run() // https://www.youtube.com/watch?v=9vz-Dcdl8JA
    {
        this.running = true;
        
        while (running)
        {
            try {
                int client = selector.select();
                
                if (client == 0) { continue; }
                
                Set<SelectionKey> keys = selector.selectedKeys();
                
                Iterator<SelectionKey> it = keys.iterator();
                
                while (it.hasNext())
                {
                    SelectionKey key = (SelectionKey)it.next();
                    
                    if ((key.readyOps() & SelectionKey.OP_ACCEPT) == SelectionKey.OP_ACCEPT)
                    {
                        Socket socket = this.ssocket.accept();
                        System.out.println("Connection from: " + socket);
                        
                        SocketChannel soch = socket.getChannel();
                        soch.configureBlocking(false);
                        soch.register(selector, SelectionKey.OP_READ);
                    } else
                    if ((key.readyOps() & SelectionKey.OP_READ) == SelectionKey.OP_READ)
                    {
                        SocketChannel soch = (SocketChannel)key.channel();
                        
                        boolean connection = readData(soch, buffer);
                        
                        if (!connection)
                        {
                            key.cancel();
                            soch.socket().close();
                        }
                    }
                    keys.clear();
                }
            } catch (IOException ioe) { ioe.printStackTrace(); }
            
            
        }
    }
    public void open()
    {
        try {
            ServerSocketChannel ssoch = ServerSocketChannel.open();
            ssoch.configureBlocking(false);
            
            this.ssocket = ssoch.socket();
            
            InetSocketAddress isa = new InetSocketAddress(port);
            ssoch.bind(isa);
            
            this.selector = Selector.open();
            
            ssoch.register(this.selector, SelectionKey.OP_ACCEPT);
            
            System.out.println("Server Started.");
        } catch (IOException ioe) { ioe.printStackTrace(); }
    }
    public boolean readData(SocketChannel soch, ByteBuffer buff)
    {
        return false;
    }
}
