package server;

import java.io.InputStream;
import java.io.OutputStream;
import java.io.PrintWriter;
import java.io.OutputStreamWriter;
import java.io.IOException;
import java.net.ServerSocket;
import java.net.Socket;
import java.util.Scanner;
import java.util.ArrayList;

public class Main implements Runnable
{
    public static final int VERSION = 1;
    public static final int PORT = 9991;
    public static final int MAXPLAYERS = 2;
    
    public ArrayList<Double> PlayerX;
    public ArrayList<Double> PlayerY;
    public ArrayList<Integer> PlayerHealth;
    public ArrayList<Integer> PlayerScore;
    public ArrayList<String> PlayerNames;
    public int PlayerCount;
    
    public static void main(String[] args)
    {
        new Thread(new Main()).start();
    }
    
    public Main()
    {
        this.PlayerX = new ArrayList<Double>();
        this.PlayerY = new ArrayList<Double>();
        this.PlayerHealth = new ArrayList<Integer>();
        this.PlayerScore = new ArrayList<Integer>();
        this.PlayerNames = new ArrayList<String>();
        this.PlayerCount = 0;
    }

    @Override
    public void run()
    {
        System.out.println("Starting BlasterSpace server version " + VERSION + " on port " + PORT);
        
        try(ServerSocket serverSocket = new ServerSocket(PORT)) {
            Socket connectionSocket = serverSocket.accept();

            while (this.PlayerCount < MAXPLAYERS)
            {
                Socket sock = serverSocket.accept();
                new Thread(new ServerThread(this, sock)).start();
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}
