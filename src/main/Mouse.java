package main;

import java.awt.event.MouseListener;
import java.awt.event.MouseEvent;

import java.awt.MouseInfo;

import javax.swing.JFrame;

public class Mouse implements MouseListener
{
	public boolean MB_1;
	public boolean MB_2;
	public boolean MB_3;

    public JFrame listenerFrame;

	@Override
	public void mousePressed(MouseEvent e)
	{
		if (e.getButton() == 1) { this.MB_1 = true; } else
		if (e.getButton() == 2) { this.MB_2 = true; } else
		if (e.getButton() == 3) { this.MB_3 = true; }
	}
	@Override
	public void mouseReleased(MouseEvent e)
	{
		if (e.getButton() == 1) { this.MB_1 = false; } else
		if (e.getButton() == 2) { this.MB_2 = false; } else
		if (e.getButton() == 3) { this.MB_3 = false; }
	}
	@Override
	public void mouseExited(MouseEvent e) {}
	public void mouseEntered(MouseEvent e) {}
	public void mouseClicked(MouseEvent e) {}

    public void setFrame(JFrame frame) { this.listenerFrame = frame; }

    public int getMouseX()
    {
        if (this.listenerFrame == null) { return 0; }

        return MouseInfo.getPointerInfo().getLocation().x - this.listenerFrame.getLocationOnScreen().x;
    }
    public int getMouseY()
    {
        if (this.listenerFrame == null) { return 0; }

        return MouseInfo.getPointerInfo().getLocation().y - this.listenerFrame.getLocationOnScreen().y;
    }
}
