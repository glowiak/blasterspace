package main;

import java.net.Socket;
import java.io.ObjectOutputStream;
import java.io.ObjectInputStream;
import java.io.IOException;

public class BSClient implements Runnable
{
    private String host;
    private int port;
    
    private Socket socket;
    private ObjectOutputStream oos;
    private ObjectInputStream ois;
    private boolean running = false;
    
    public BSClient(String host, int port)
    {
        this.host = host;
        this.port = port;
    }
    
    public void connect()
    {
        try {
            this.socket = new Socket(this.host, this.port);
            this.oos = new ObjectOutputStream(this.socket.getOutputStream());
            this.ois = new ObjectInputStream(this.socket.getInputStream());
            new Thread(this).start();
        } catch (IOException ioe) { ioe.printStackTrace(); }
    }
    
    @Override
    public void run()
    {
    }
}
