package main;

public class Player extends Entity
{
    public boolean isShooting;
    public boolean isHit;
    public int cooldown;
    public int hitCooldown;
    
    public int score;
    public int maxHealth;
    
    public Player() {
        super();
        
        this.texture = Texture.PLAYER;
        this.x = (double)(Main.WIDTH/2);
        this.y = (double)(Main.HEIGHT - 40);
        this.isShooting = false;
        this.isHit = false;
        this.cooldown = 0;
        this.hitCooldown = 0;
        this.score = 0;
        this.health = 4;
        this.damage = 1;
        this.maxHealth = 10;
    }
    
    public void move(double much)
    {
        if (much < 0 && this.x > 0 || much > 0 && this.x < Main.WIDTH - 32)
        { this.x += much; }
    }
    
    @Override
    public void loop()
    {   
        if (this.isShooting && this.cooldown == 0)
        {
            this.texture = Texture.PLAYER_SHOOT;
            new Bullet();
            this.cooldown = 50;
        }
        if (this.cooldown <= 35) { this.texture = Texture.PLAYER; }
        if (this.cooldown > 0) { this.cooldown--; }
        if (this.hitCooldown > 0) { this.hitCooldown--; }
        
        if (this.health <= 0)
        {
            this.texture = Texture.PLAYER_DEAD;
        }
        
        for (int i = 0; i < Main.r.AlienBullets.size(); i++)
        {            
            if (Main.r.AlienBullets.get(i).x >= this.x && Main.r.AlienBullets.get(i).x <= this.x + 32 && Main.r.AlienBullets.get(i).y >= this.y && Main.r.AlienBullets.get(i).y <= this.y + 32)
            {
                this.isHit = true;
                Main.r.AlienBullets.get(i).y = -999;
                health -= Main.r.AlienBullets.get(i).owner.damage;
                
                new AlienParticle(-0.1, -0.1, this.x, this.y);
                new AlienParticle(0.0, -0.1, this.x + 16, this.y);
                new AlienParticle(0.1, -0.1, this.x + 32, this.y);
                
                new AlienParticle(0.1, 0.1, this.x, this.y + 32);
                new AlienParticle(0.0, 0.1, this.x + 16, this.y + 32);
                new AlienParticle(0.1, 0.1, this.x + 32, this.y + 32);
                
                new AlienParticle(-0.1, 0.0, this.x, this.y + 16);
                new AlienParticle(0.0, 0.1, this.x + 32, this.y + 16);
            }
        }
    }
}
