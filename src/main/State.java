package main;

public class State
{
    public static final int MENU = 0;
    public static final int OPTIONS = 2;
    public static final int ABOUT = 3;
    public static final int GAME = 1;
    public static final int SHOP = 4;
    
    public static int CURRENT = State.MENU;
}
