package main;

import java.awt.Image;

public abstract class Entity
{
    public double x;
    public double y;
    public int health;
    public int damage;
    public Image texture;
    
    public void loop()
    {
    }
}
