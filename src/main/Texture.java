package main;

import java.awt.Image;
import javax.swing.ImageIcon;

public class Texture
{
    public static final Main obj = new Main();
    
    public static final Image BACKGROUND = new ImageIcon(obj.getClass().getResource("/resources/bgnd.png")).getImage();
    public static final Image PLAYER = new ImageIcon(obj.getClass().getResource("/resources/player.png")).getImage();
    public static final Image PLAYER_SHOOT = new ImageIcon(obj.getClass().getResource("/resources/plrshoot.png")).getImage();
    public static final Image PLAYER_HIT = new ImageIcon(obj.getClass().getResource("/resources/plrhit.png")).getImage();
    public static final Image PLAYER_DEAD = new ImageIcon(obj.getClass().getResource("/resources/plrdead.png")).getImage();
    public static final Image PLAYER_BULLET = new ImageIcon(obj.getClass().getResource("/resources/pbullet.png")).getImage();
    public static final Image HEART = new ImageIcon(obj.getClass().getResource("/resources/heart.png")).getImage();
    public static final Image ALIEN1 = new ImageIcon(obj.getClass().getResource("/resources/enemy1.png")).getImage();
    public static final Image ALIEN1_1 = new ImageIcon(obj.getClass().getResource("/resources/enemy1_1.png")).getImage();
    public static final Image ALIEN2 = new ImageIcon(obj.getClass().getResource("/resources/enemy2.png")).getImage();
    public static final Image ALIEN2_1 = new ImageIcon(obj.getClass().getResource("/resources/enemy2_1.png")).getImage();
    public static final Image ALIEN_BULLET = new ImageIcon(obj.getClass().getResource("/resources/ebullet.png")).getImage();
    public static final Image RETRY = new ImageIcon(obj.getClass().getResource("/resources/retry.png")).getImage();
    public static final Image PARTICLE = new ImageIcon(obj.getClass().getResource("/resources/part1.png")).getImage();
    public static final Image APARTICLE = new ImageIcon(obj.getClass().getResource("/resources/part2.png")).getImage();
    public static final Image PAUSED = new ImageIcon(obj.getClass().getResource("/resources/paused.png")).getImage();
    public static final Image ALIEN3 = new ImageIcon(obj.getClass().getResource("/resources/enemy3.png")).getImage();
    public static final Image ALIEN3_1 = new ImageIcon(obj.getClass().getResource("/resources/enemy3_1.png")).getImage();
    public static final Image MESSAGE = new ImageIcon(obj.getClass().getResource("/resources/message.png")).getImage();
    public static final Image LOGO = new ImageIcon(obj.getClass().getResource("/resources/logo_transparent.png")).getImage();
}
