package main;

public class AlienBullet extends Entity
{
    public double movx;
    public Alien owner;
    
    public AlienBullet(Alien owner, double x, double y, double movx)
    {
        super();
        
        this.texture = Texture.ALIEN_BULLET;
        this.x = x;
        this.y = y;
        this.movx = movx;
        this.owner = owner;
        
        Main.r.Sprites.add(this);
        Main.r.AlienBullets.add(this);
    }
    
    @Override
    public void loop()
    {   
        this.y += 0.2;
        this.x += this.movx;
    }
}
