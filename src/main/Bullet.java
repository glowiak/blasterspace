package main;

public class Bullet extends Entity
{
    public Bullet()
    {
        super();
        
        this.texture = Texture.PLAYER_BULLET;
        this.x = Main.r.plr.x + 11.7;
        this.y = Main.r.plr.y;
        
        Main.r.Sprites.add(this);
        Main.r.Bullets.add(this);
    }
    
    @Override
    public void loop()
    {
        this.y -= 0.5;
    }
}
