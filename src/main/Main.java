package main;

import javax.swing.JFrame;

import java.util.Random;

public class Main implements Runnable
{
    public static final int BlasterSpace_VERSION_MAJOR = 0;
    public static final int BlasterSpace_VERSION_MINOR = 0;
    public static final int BlasterSpace_VERSION_HOTFIX = 7;
    
    public static JFrame w = new JFrame("BlasterSpace v" + BlasterSpace_VERSION_MAJOR + "." + BlasterSpace_VERSION_MINOR + "." + BlasterSpace_VERSION_HOTFIX);
    public static Renderer r = new Renderer();
    
    public static final int WIDTH = 320;
    public static final int HEIGHT = 240;
    
    public static int fps = 0;
    public static final int wantedFPS = 315;
    
    public static int baseAlienSpawnX = 32;
    public static int baseAlienSpawnY = 10;
    
    public static double lastAlienX;
    public static double lastAlienY;
    
    public static int maxAlienSpawn = 40;
    public static boolean canTakeScreenshoot = true;
    
    public static int optionSel = 0;
    public static int clickCooldown = 0;
    public static int pauseCooldown = 0;
    public static int screenCooldown = 0;
    
    public static double SCALE_X;
    public static double SCALE_Y;
    
    public static void main(String[] args)
    {
        new Thread(new Main()).start();
    }
    
    @Override
    public void run()
    {
        w.setIconImage(Texture.PLAYER);
        w.setSize(800, 700);
        w.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        w.add(r);
        w.setVisible(true);

        double drawInterval = 1000000000/wantedFPS;

        boolean shouldRender = false;
        long lastTime = System.nanoTime();
        long staticTime = System.nanoTime();
        long SECOND = 1000000000L;
        double unprocessed = 0.0;
        int frames = 0;
        double nextDrawTime = lastTime + drawInterval;
        
        for (int i = 0; i < new Random().nextInt(maxAlienSpawn - 3) + 3; i++)
        { new Alien(); }
        
        while (true)
        {
            SCALE_X = (double)(w.getSize().width / WIDTH);
            SCALE_Y = (double)(w.getSize().height / HEIGHT);
            
            long currentTime = System.nanoTime();
            long passedTime = currentTime - lastTime;
            
            double remainingTime = (nextDrawTime - currentTime)/1000000;
            
            if (currentTime - staticTime > SECOND)
            {
                fps = frames;
                frames = 0;
                staticTime += SECOND;;
            }
            lastTime = currentTime;
            unprocessed += passedTime/(double)SECOND;
            
            while (unprocessed >= 1)
            {
                shouldRender = true;
                unprocessed -= 1;
            }
            
            nextDrawTime += drawInterval;
            
            if (remainingTime < 0) { remainingTime = 0; }
            
            try { Thread.sleep((long)remainingTime); } catch (InterruptedException ie) { ie.printStackTrace(); }
            
            if (shouldRender)
            {
                frames++;
                
                if (r.keyboard.K_ESCAPE) { System.exit(0); }
                
                        
                if (!r.isPaused || State.CURRENT != State.GAME)
                {
                    r.bgy1 += 0.2;
                    r.bgy2 += 0.2;
                }
                        
                if (r.bgy1 >= HEIGHT) { r.bgy1 = -HEIGHT; }
                if (r.bgy2 >= HEIGHT) { r.bgy2 = -HEIGHT; }
                
                switch (State.CURRENT)
                {
                    case State.MENU:
                        if (optionSel == 0 && r.keyboard.K_DOWN && clickCooldown == 0)
                        { clickCooldown = 50; optionSel = 1; }
                        if (optionSel == 1 && r.keyboard.K_UP && clickCooldown == 0)
                        { clickCooldown = 50; optionSel = 0; }
                        if (optionSel == 1 && r.keyboard.K_DOWN && clickCooldown == 0)
                        { clickCooldown = 50; optionSel = 2; }
                        if (optionSel == 2 && r.keyboard.K_UP && clickCooldown == 0)
                        { clickCooldown = 50; optionSel = 1; }
                    
                        if (r.mouse.getMouseX() >= (100*SCALE_X) && r.mouse.getMouseY() >= (120*SCALE_Y) && r.mouse.getMouseX() <= (220*SCALE_X) && r.mouse.getMouseY() <= (140*SCALE_Y) && r.mouse.MB_1)
                        {
                            optionSel = 0;
                            State.CURRENT = State.GAME;
                        }
                    
                        if (r.keyboard.K_ENTER && optionSel == 0) { State.CURRENT = State.GAME; }
                        if (r.keyboard.K_ENTER && optionSel == 2) { State.CURRENT = State.ABOUT; }
                        
                        if (clickCooldown > 0) { clickCooldown--; }
                        break;
                    case State.ABOUT:
                        if (r.keyboard.K_SPACE) { State.CURRENT = State.MENU; }
                        break;
                    case State.SHOP:
                        if (r.keyboard.K_E) { State.CURRENT = State.GAME; }
                        break;
                    case State.GAME:
                        if (r.keyboard.K_LEFT && r.plr.health > 0 && !r.isPaused) { r.plr.move(-0.4); }
                        if (r.keyboard.K_RIGHT && r.plr.health > 0 && !r.isPaused) { r.plr.move(0.4); }
                        
                        if (r.keyboard.K_SPACE && r.plr.health > 0 && !r.isPaused) { r.plr.isShooting = true; } else { r.plr.isShooting = false; }
                        
                        if (r.keyboard.K_P && pauseCooldown == 0)
                        {
                            pauseCooldown = 100;
                            if (r.isPaused)
                            { r.isPaused = false; } else
                            { r.isPaused = true; }
                        }
                        
                        if (r.keyboard.K_M && r.isPaused)
                        {
                            State.CURRENT = State.MENU;
                        }
                        if (pauseCooldown > 0) { pauseCooldown--; }
                        
                        if (r.enemyCount <= 0)
                        {
                            r.currentLevel++;
                            r.plr.health++;
                            baseAlienSpawnX = 32;
                            baseAlienSpawnY = 10;
                            
                            r.Bullets.clear();
                            r.AlienBullets.clear();
                            r.Aliens.clear();
                            r.Sprites.clear();
                            
                            r.Sprites.add(r.plr);
                            
                            new Particle(-0.1, -0.1, lastAlienX, lastAlienY);
                            new Particle(0.0, -0.1, lastAlienX + 16, lastAlienY);
                            new Particle(0.1, -0.1, lastAlienX + 32, lastAlienY);
                
                            new Particle(0.1, 0.1, lastAlienX, lastAlienY + 32);
                            new Particle(0.0, 0.1, lastAlienX + 16, lastAlienY + 32);
                            new Particle(0.1, 0.1, lastAlienX + 32, lastAlienY + 32);
                            
                            new Particle(-0.1, 0.0, lastAlienX, lastAlienY + 16);
                            new Particle(0.0, 0.1, lastAlienX + 32, lastAlienY + 16);
                            
                            maxAlienSpawn += 3;
                            
                            for (int i = 0; i < new Random().nextInt(maxAlienSpawn - 3) + 3; i++)
                            { new Alien(); }
                        }
                        
                        for (int i = 0; i < r.Sprites.size(); i++)
                        {
                            if(!r.isPaused)
                            { r.Sprites.get(i).loop(); }
                        }
                        
                        for (int i = 0; i < r.Messages.size(); i++)
                        {
                            if(!r.isPaused)
                            { r.Messages.get(i).loop(); }
                        }
                        
                        if (r.plr.health <= 0 && r.retryY < HEIGHT/2 - 32 && !r.isPaused)
                        {
                            r.retryY++;
                        }
                        if (r.isPaused && r.pausedY < HEIGHT/2 - 32)
                        {
                            r.pausedY++;
                        }
                        if (!r.isPaused && r.pausedY > 0)
                        {
                            r.pausedY--;
                        }
                        if (r.plr.health > 0 && r.retryY > 0)
                        {
                            r.retryY--;
                        }
                        if (r.plr.health <= 0 && r.keyboard.K_R)
                        {
                            r.currentLevel = -1;
                            maxAlienSpawn = 40;
                            r.plr.score = 0;
                            r.plr.health = 3;
                            for (int i = 0; i < r.Aliens.size(); i++)
                            {
                                r.Aliens.get(i).y = 999;
                            }
                            r.enemyCount = 0;
                        }
                        if (screenCooldown == 0 && canTakeScreenshoot && r.keyboard.K_F2)
                        {
                            screenCooldown = 500;
                        }
                        if (screenCooldown == 0 && !canTakeScreenshoot)
                        {
                            canTakeScreenshoot = true;
                        }
                        if (screenCooldown > 0) { screenCooldown--; }
                        
                        if (r.plr.health > r.plr.maxHealth) { r.plr.health = r.plr.maxHealth; }
                        break;
                }
                
                r.repaint();
            }
        }
    }
}
