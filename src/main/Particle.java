package main;

import java.util.Random;

public class Particle extends Entity
{
    public double speedx;
    public double speedy;
    
    public int cooldown;
    
    public Particle(double speedx, double speedy, double x, double y)
    {
        super();
        
        this.texture = Texture.PARTICLE;
        this.x = x;
        this.y = y;
        this.speedx = speedx;
        this.speedy = speedy;
        this.cooldown = new Random().nextInt(250 - 50) + 50;
        
        Main.r.Sprites.add(this);
    }
    
    @Override
    public void loop()
    {
        if (this.y < 999)
        {
            if (this.cooldown > 0)
            {
                this.x += this.speedx;
                this.y += this.speedy;
                cooldown--;
            } else
            if (this.cooldown <= 0)
            {
                this.y = 999;
            }
        }
    }
}
