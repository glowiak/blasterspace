package main;

import java.util.Random;

public class Alien extends Entity
{
    public int moveCount;
    public double speed;
    public double willShoot;
    public int cooldown;
    public int skinCooldown;
    public int skinNumber;
    
    public Alien()
    {
        super();
        
        int rnd1 = new Random().nextInt(200);
        if (rnd1 <= 60)
        { this.skinNumber = 0; } else if (rnd1 > 60 && rnd1 <= 85)
        { this.skinNumber = 1; } else if (Main.r.currentLevel >= 20 && rnd1 > 85 && rnd1 <= 105)
        { this.skinNumber = 2; }
        
        if (this.skinNumber == 0)
        { this.texture = Texture.ALIEN1; } else if (this.skinNumber == 1)
        { this.texture = Texture.ALIEN2; } else if (this.skinNumber == 2)
        { this.texture = Texture.ALIEN3; }
        this.x = (double)Main.baseAlienSpawnX;
        this.y = (double)Main.baseAlienSpawnY;
        
        if (this.skinNumber == 0) { this.willShoot = 2.0; this.health = 1; } else
        if (this.skinNumber == 1) { this.willShoot = 0.8; this.health = 5; } else
        if (this.skinNumber == 2) { this.willShoot = 1.0; this.health = 15; }
        
        this.moveCount = 0;
        this.speed = 0.4;
        if (this.skinNumber == 2) { this.damage = 3; } else
        { this.damage = 1; }
        
        this.cooldown = 0;
        this.skinCooldown = 0;
        
        Main.baseAlienSpawnX += 32;
        if (Main.baseAlienSpawnX > Main.WIDTH - 32)
        {
            Main.baseAlienSpawnX = 32;
            Main.baseAlienSpawnY += 32;
        }
        
        Main.r.Sprites.add(this);
        Main.r.Aliens.add(this);
        Main.r.enemyCount++;
    }
    
    @Override
    public void loop()
    {
        if (this.y >= Main.r.plr.y && this.y < 999.0) { Main.r.plr.health = 0; }
        
        this.x += this.speed;
        if (this.x >= Main.WIDTH - 32)
        {
            this.speed = -0.4;
            this.y += 5;
        }
        if (this.x <= 0)
        {
            this.speed = 0.4;
            this.y += 5;
        }
        
        if (this.y > 0 && this.y < 999)
        {
            double shootChance = 0.0 + new Random().nextDouble() * 1000.0;
            
            if (shootChance <= this.willShoot && this.cooldown == 0)
            {
                double movx = 0.0;
                if (this.x < Main.r.plr.x)
                {
                    movx = 0.1;
                }
                if (this.x > Main.r.plr.x)
                {
                    movx = -0.1;
                }
                
                new AlienBullet(this, this.x + 16.0, this.y + 16.0, movx);
                this.cooldown = 100;
            }
            
            if (this.skinCooldown == 0 && this.skinNumber == 0)
            {
                this.skinCooldown = 100;
                this.texture = Texture.ALIEN1_1;
            }
            if (this.skinCooldown <= 35 && this.skinNumber == 0)
            {
                this.texture = Texture.ALIEN1;
            }
            
            if (this.skinCooldown == 0 && this.skinNumber == 1)
            {
                this.skinCooldown = 100;
                this.texture = Texture.ALIEN2_1;
            }
            if (this.skinCooldown <= 35 && this.skinNumber == 1)
            {
                this.texture = Texture.ALIEN2;
            }
            if (this.skinCooldown == 0 && this.skinNumber == 2)
            {
                this.skinCooldown = 100;
                this.texture = Texture.ALIEN3_1;
            }
            if (this.skinCooldown <= 35 && this.skinNumber == 2)
            {
                this.texture = Texture.ALIEN3;
            }
            
            if (this.cooldown > 0) { this.cooldown--; }
            if (this.skinCooldown > 0) { this.skinCooldown--; }
        }
        
        for (int i = 0; i < Main.r.Bullets.size(); i++)
        {
            if (Main.r.Bullets.get(i).x >= this.x && Main.r.Bullets.get(i).x <= this.x + 32 && Main.r.Bullets.get(i).y <= this.y + 32 && Main.r.Bullets.get(i).y >= this.y)
            {
                this.health -= Main.r.plr.damage;
                
                double lastX = this.x;
                double lastY = this.y;
                Main.r.Bullets.get(i).y = -999;
                
                if (this.health <= 0)
                {
                    this.y = 999;
                    Main.r.enemyCount--;
                    Main.r.plr.score += new Random().nextInt(900 - 400) + 400;
                    
                    if (this.skinNumber == 2)
                    {
                        Main.r.plr.health++;
                    }
                }
                
                new Particle(-0.1, -0.1, lastX, lastY);
                new Particle(0.0, -0.1, lastX + 16, lastY);
                new Particle(0.1, -0.1, lastX + 32, lastY);
                
                new Particle(0.1, 0.1, lastX, lastY + 32);
                new Particle(0.0, 0.1, lastX + 16, lastY + 32);
                new Particle(0.1, 0.1, lastX + 32, lastY + 32);
                
                new Particle(-0.1, 0.0, lastX, lastY + 16);
                new Particle(0.0, 0.1, lastX + 32, lastY + 16);
                
                Main.lastAlienX = lastX;
                Main.lastAlienY = lastY;
            }
        }
    }
}
