package main;

import java.awt.Image;
import java.awt.image.BufferedImage;

import java.awt.Graphics;
import java.awt.Color;

public class Message extends Entity
{
    public int cooldown;
    public boolean isClosed;
    
    public Message(String msg)
    {
        super();
        this.x = 100;
        this.y = 75;
        this.cooldown = 1000;
        this.isClosed = false;
        
        BufferedImage tex = new BufferedImage(150, 40, BufferedImage.TYPE_INT_RGB);
        Graphics tg = tex.getGraphics();
        
        tg.drawImage(Texture.MESSAGE, 0, 0, null);
        tg.setColor(Color.yellow);
        tg.drawString(msg, 15, 25);
        
        this.texture = (Image)tex;
        
        Main.r.Messages.add(this);
    }
    
    @Override
    public void loop()
    {
        if (this.cooldown > 0) { this.cooldown--; }
        
        if (this.cooldown <= 0)
        {
            if (this.y > -155)
            {
                this.y -= 0.5;
            }
            this.isClosed = true;
        }
    }
}
