package main;

import javax.swing.JPanel;

import javax.imageio.ImageIO;

import java.io.File;
import java.io.IOException;

import java.awt.Graphics;
import java.awt.Dimension;
import java.awt.Color;
import java.awt.Image;

import java.awt.image.BufferedImage;

import java.util.ArrayList;
import java.util.Random;

public class Renderer extends JPanel
{
    public Keyboard keyboard;
    public Mouse mouse;
    
    public ArrayList<Entity> Sprites;
    public ArrayList<Bullet> Bullets;
    public ArrayList<Alien> Aliens;
    public ArrayList<AlienBullet> AlienBullets;
    public ArrayList<Message> Messages;
    
    public int enemyCount;
    public int retryY;
    public int pausedY;
    public int currentLevel;
    
    public boolean isPaused;
    
    public Player plr;
    
    public double bgy1;
    public double bgy2;
    
    public Renderer()
    {
        this.keyboard = new Keyboard();
        this.mouse = new Mouse();
        
        this.mouse.setFrame(Main.w);
        
        this.setBackground(Color.black);
        this.setDoubleBuffered(true);
        this.setFocusable(true);
        this.addKeyListener(this.keyboard);
        this.addMouseListener(this.mouse);
        
        this.Sprites = new ArrayList<Entity>();
        this.Bullets = new ArrayList<Bullet>();
        this.Aliens = new ArrayList<Alien>();
        this.AlienBullets = new ArrayList<AlienBullet>();
        this.Messages = new ArrayList<Message>();
        this.plr = new Player();
        
        this.Sprites.add(this.plr);
        this.enemyCount = 0;
        this.retryY = 0;
        this.pausedY = 0;
        this.currentLevel = 0;
        
        this.bgy1 = -Main.HEIGHT;
        this.bgy2 = 0;
    }
    
    public void paintComponent(Graphics ge)
    {
        BufferedImage bi = new BufferedImage(Main.WIDTH, Main.HEIGHT, BufferedImage.TYPE_INT_RGB);
        Graphics g = bi.getGraphics();
        
        switch (State.CURRENT)
        {
            case State.MENU:
                g.drawImage(Texture.BACKGROUND, 0, (int)this.bgy1, null);
                g.drawImage(Texture.BACKGROUND, 0, (int)this.bgy2, null);
                
                g.drawImage(Texture.LOGO, 11, 11, null);
                
                if (Main.optionSel == 0)
                { g.setColor(Color.white); } else { g.setColor(Color.gray); }
                
                g.drawRect(100, 70, 120, 20);
                g.drawString("PLAY", 150, 85);
                
                if (Main.optionSel == 1)
                { g.setColor(Color.white); } else { g.setColor(Color.gray); }
                
                g.drawRect(100, 100, 120, 20);
                g.drawString("OPTIONS", 135, 115);
                
                if (Main.optionSel == 2)
                { g.setColor(Color.white); } else { g.setColor(Color.gray); }
                
                g.drawRect(100, 130, 120, 20);
                g.drawString("ABOUT", 145, 145);
                break;
            case State.ABOUT:
                g.drawImage(Texture.BACKGROUND, 0, (int)this.bgy1, null);
                g.drawImage(Texture.BACKGROUND, 0, (int)this.bgy2, null);
                g.drawImage(Texture.LOGO, 11, 11, null);
                g.setColor(Color.white);
                g.drawString("BlasterSpace is a retro styled space", 10, 85);
                g.drawString("shooter made by glowiak", 10, 100);
                g.drawString("Press SPACE to return to menu.", 10, Main.HEIGHT - 20);
                break;
            case State.SHOP:
                g.drawImage(Texture.BACKGROUND, 0, (int)this.bgy1, null);
                g.drawImage(Texture.BACKGROUND, 0, (int)this.bgy2, null);
                g.drawString("SHOP", Main.WIDTH/2 - 10, 10);
                g.drawString("" + this.plr.score, 1, 10);
                g.drawString("PRESS E TO EXIT", Main.WIDTH/2 - 50, Main.HEIGHT - 15);
                break;
            case State.GAME:
                g.drawImage(Texture.BACKGROUND, 0, (int)this.bgy1, null);
                g.drawImage(Texture.BACKGROUND, 0, (int)this.bgy2, null);
                
                
                for (int i = 0; i < this.Sprites.size(); i++)
                {
                    g.drawImage(this.Sprites.get(i).texture, (int)this.Sprites.get(i).x, (int)this.Sprites.get(i).y, null);
                }
                
                for (int i = 0; i < this.Messages.size(); i++)
                {
                    g.drawImage(this.Messages.get(i).texture, (int)this.Messages.get(i).x, (int)this.Messages.get(i).y, null);
                }
                
                int base_x = 1;
                for (int i = 0; i < this.plr.health; i++)
                {
                    g.drawImage(Texture.HEART, base_x, 15, null);
                    base_x += 10;
                }
                
                g.setColor(Color.yellow);
                g.drawString("" + this.plr.score, 1, 10);
                g.drawString("Level " + this.currentLevel, Main.WIDTH/2 - 25, 10);
                
                if (plr.isHit && plr.hitCooldown == 0)
                {
                    plr.isHit = false;
                    plr.hitCooldown = 50;
                }
                if (plr.hitCooldown >= 35 && plr.health > 0)
                {
                    g.drawImage(Texture.PLAYER_HIT, (int)plr.x, (int)plr.y, null);
                }
                
                if (plr.health <= 0 || this.retryY > 0)
                {
                    g.drawImage(Texture.RETRY, Main.WIDTH/2 - 120, this.retryY, null);
                }
                if (isPaused || this.pausedY > 0)
                {
                    g.drawImage(Texture.PAUSED, Main.WIDTH/2 - 120, this.pausedY, null);
                }
                
                if (keyboard.K_F3)
                {
                    g.setColor(Color.white);
                    g.drawString("FPS: " + Main.fps, Main.WIDTH - 75, 12);
                }
                if (keyboard.K_F2 && Main.canTakeScreenshoot)
                {
                    Main.canTakeScreenshoot = false;
                    Image toPrint = bi.getScaledInstance(Main.w.getSize().width, Main.w.getSize().height, Image.SCALE_DEFAULT);
                    BufferedImage tp1 = new BufferedImage(Main.w.getSize().width, Main.w.getSize().height, BufferedImage.TYPE_INT_RGB);
                    tp1.getGraphics().drawImage(toPrint, 0, 0, null);
                    File toSave = new File("./" + "screenshoot" + new Random().nextInt(9999) + ".png");
                    try {
                        ImageIO.write(tp1, "png", toSave);
                    } catch (IOException ioe) { ioe.printStackTrace(); }
                    
                    new Message("Screenshoot taken!");
                }
                
                break;
        }

        ge.drawImage(bi.getScaledInstance(Main.w.getSize().width, Main.w.getSize().height, Image.SCALE_DEFAULT), 0, 0, null);
    }
}
