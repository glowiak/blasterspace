#!/bin/sh
VMA=$(cat src/main/Main.java | grep "BlasterSpace_VERSION_MAJOR = " | sed 's/    public static final int BlasterSpace_VERSION_MAJOR = //' | sed 's/;//')
VMI=$(cat src/main/Main.java | grep "BlasterSpace_VERSION_MINOR = " | sed 's/    public static final int BlasterSpace_VERSION_MINOR = //' | sed 's/;//')
VHO=$(cat src/main/Main.java | grep "BlasterSpace_VERSION_HOTFIX = " | sed 's/    public static final int BlasterSpace_VERSION_HOTFIX = //' | sed 's/;//')

VERSION=$VMA.$VMI.$VHO
TARGET=blasterspace-$VERSION.jar

BUILD_SERVER=${BUILD_SERVER:-FALSE}
BUILD_CLIENT=${BUILD_CLIENT:-TRUE}

rm -f $TARGET.jar
if [ "$BUILD_CLIENT" == "TRUE" ]; then javac src/main/*.java; fi # the client
if [ "$BUILD_SERVER" == "TRUE" ]; then javac src/server/*.java; fi # the server
(cd src && jar cvfe ../$TARGET main.Main `find . -name '*.class'` resources/)
