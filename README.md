# BlasterSpace

A retro styled space shooter

### Building from source

Both to build and run, Java 8 or higher is needed.

    sh ./build.sh

This will generate the executable ./blasterspace-version.jar
